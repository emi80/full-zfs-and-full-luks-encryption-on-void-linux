#!/bin/bash

# message to user
echo "This script sets up the need bind mounts for the chroot, and copies over necessary files (including a script to import relevant variables.)"

for i in /dev /dev/pts /proc /sys
do
  echo -n "mounting $i..."
  mount -B $i ${TARGET}$i
  echo ' completed.'
done

cp -p /etc/resolv.conf ${TARGET}/etc/ # copy DNS resolver configuration
cp /etc/hostid ${TARGET}/etc/ # copy hostid onto actual system

# copy cryptuuid & luksname into chroot location
echo "export DEVICE=${DEVICE}" > ${TARGET}/importvars.sh
echo "export CRYPTUUID=${CRYPTUUID}" >> ${TARGET}/importvars.sh
echo "export LUKSNAME=${LUKSNAME}" >> ${TARGET}/importvars.sh
echo "export ZPOOLNAME=${ZPOOLNAME}" >> ${TARGET}/importvars.sh
echo "export USERNAME=${USERNAME}" >> ${TARGET}/importvars.sh
echo "export ROTATIONAL=${ROTATIONAL}" >> ${TARGET}/importvars.sh

cp 06-config-inside-chroot.sh ${TARGET}
cp 08-post-installation-setup.sh ${TARGET}

echo "After you enter <return> you will be in the Void chroot mounted on ${TARGET}. You should execute the next script in the chroot; entering in the terminal:"
echo ". 06-config-inside-chroot.sh"

read -p "Please press <return> to continue." enterchroot
chroot ${TARGET}

